// ***********************************************
// This example commands.js shows you how to
// create various custom commands and overwrite
// existing commands.
//
// For more comprehensive examples of custom
// commands please read more here:
// https://on.cypress.io/custom-commands
// ***********************************************
//
//
// -- This is a parent command --
// Cypress.Commands.add('login', (email, password) => { ... })
//
//
// -- This is a child command --
// Cypress.Commands.add('drag', { prevSubject: 'element'}, (subject, options) => { ... })
//
//
// -- This is a dual command --
// Cypress.Commands.add('dismiss', { prevSubject: 'optional'}, (subject, options) => { ... })
//
//
// -- This will overwrite an existing command --
// Cypress.Commands.overwrite('visit', (originalFn, url, options) => { ... })

Cypress.Commands.add("login",(number,otp) => {
    cy.request("POST","https://staging.quillplus.in/api/v1/users/gen-otp",{
      "phone": number,
      "device": "web"
    }).then((response) => {
      cy.request("POST","https://staging.quillplus.in/api/v1/users/verify-otp",{
        "phone": number,
        "device":"web",
        "otp": otp,
        "token": response.body.data.token
      }).then(response => {
        return response.body.data.token
      })
    })
})