before("Login", () => {
    cy.login("","998877").then((token) => {
        Cypress.env("token",token)
        return token
    }).then((token) => {
        cy.request({
            method: "GET",
            url: "https://staging.quillplus.in/api/v1/users/user?device=web",
            headers: {
                Authorization: token
            }
        }).then((response) => {
            for(let i=0;i<response.body.data.academicInfo.data.length;i++){
                if(response.body.data.academicInfo.data[i].name === "MHT-CET"){
                    Cypress.env({
                        MHT_CET: {
                            standard_id: response.body.data.academicInfo.data[i].id
                        }
                    })
                }
                if(response.body.data.academicInfo.data[i].name === "JEE"){
                    Cypress.env({
                        JEE: {
                            standard_id: response.body.data.academicInfo.data[i].id
                        }
                    })
                }
                if(response.body.data.academicInfo.data[i].name === "NEET"){
                    Cypress.env({
                        NEET: {
                            standard_id: response.body.data.academicInfo.data[i].id
                        }
                    })
                }
                Cypress.env("whitelabel_id",response.body.data.currentWhiteLabel.data.id)
            }
        })
    })
})