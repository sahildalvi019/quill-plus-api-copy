///<reference types="Cypress" />
describe('Practice Subjects', () => {
    it('Validate practice subjects', () => {
        cy.request({
        method: "GET",
        url: "https://staging.quillplus.in/api/v1/practice/subjects?standard_id=7&whitelabel_id=2&device=web",
        headers: {
            Authorization: Cypress.env("token")
        }
        }).then((subjectsResponse) => {
            let subjectNames = []

            expect(subjectsResponse.status).to.be.equal(200)
            for(let i=0;i<subjectsResponse.body.data.length;i++){
                subjectNames[i] = subjectsResponse.body.data[i].name.toLowerCase()
                expect(subjectsResponse.body.data[i].color).not.to.be.empty.and.not.equal(null)
                expect(subjectsResponse.body.data[i].id).to.be.a('number').and.not.equal(null)
                expect(subjectsResponse.body.data[i].num_of_questions).to.be.a('number').and.not.equal(null)
                expect(subjectsResponse.body.data[i].progress).not.equal(null)
                expect(subjectsResponse.body.data[i].subtitle).not.equal(null)
                expect(subjectsResponse.body.data[i].unique_questions_attempted).to.be.a('number').and.not.equal(null)
            }

            expect(subjectNames).contains("physics")
            expect(subjectNames).contains("chemistry")
            expect(subjectNames).contains("maths")
            expect(subjectNames).contains("biology")
        })
    })
})