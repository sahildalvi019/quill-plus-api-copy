const { Utils } = require("../../utils/Utils")

describe("Attempt Practice Test", () => {
    it("Attempt Test", () => {
        const token = Cypress.env("token")
        const standard_id = Cypress.env("MHT_CET").standard_id
        const whitelabel_id = Cypress.env("whitelabel_id")

        cy.request({
            method: "GET",
            url: `https://staging.quillplus.in/api/v1/practice/subjects?standard_id=${standard_id}&whitelabel_id=${whitelabel_id}&device=web`,
            headers: {
                Authorization: token
            }
        }).then((subjectsResponse) => {
            let subjectData = {}
            let randomNumber = Utils.getRandomInt(0,(subjectsResponse.body.data.length-1))
            subjectData.id = subjectsResponse.body.data[randomNumber].id
            subjectData.name = subjectsResponse.body.data[randomNumber].name
            return subjectData
        }).then((subjectData) => {
            cy.request({
                method: "GET",
                url: `https://staging.quillplus.in/api/v1/practice/subject/${subjectData.id}/chapters?device=web`,
                headers: {
                    Authorization: token
                }
            }).then((chaptersResponse) => {
                let chapterIDs = []

                for(let i=0;i<chaptersResponse.body.data.units.data.length;i++){
                    for(let j=0;j<chaptersResponse.body.data.units.data[i].chapters.data.length;j++){
                        chapterIDs[j] = chaptersResponse.body.data.units.data[i].chapters.data[j].id
                    }
                }
                return chapterIDs
            }).then((chapterIDs) => {
                const numberOfQuestions = [10,20,30]
                const numberOfQuestionsSelected = numberOfQuestions[Utils.getRandomInt(0,numberOfQuestions.length-1)]
                const chapterID = chapterIDs[Utils.getRandomInt(0,chapterIDs.length-1)]
                cy.request({
                    method: "POST",
                    url: "https://staging.quillplus.in/api/v1/practice/generate-test",
                    headers: {
                        Authorization: token
                    },
                    body: {
                        "num_questions": numberOfQuestionsSelected,
                        "chapters": [chapterID],
                        "unique_questions": false,
                        "subject_id": subjectData.id,
                        "standard_id": standard_id,
                        "device": "web"
                    }
                }).then((generateTestResponse) => {
                    let testId = generateTestResponse.body.data.id
                    let testData = {}
                    let questionIDs = []
                    let optionIDs = []
                    let totalQuestions = generateTestResponse.body.data.questions.data.length

                    for(let i=0;i<generateTestResponse.body.data.questions.data.length;i++){
                        let tempOptions = []
                        questionIDs.push(generateTestResponse.body.data.questions.data[i].id)
                        for(let j=0;j<generateTestResponse.body.data.questions.data[i].options.data.length;j++){
                            tempOptions.push(generateTestResponse.body.data.questions.data[i].options.data[j].id)
                        }
                        optionIDs.push(tempOptions)
                    }

                    testData.questions = questionIDs
                    testData.options = optionIDs
                    testData.id = testId
                    testData.totalQuestions = totalQuestions
                    return testData
                }).then((testData) => {
                    for(let i=0;i<testData.totalQuestions;i++){
                        let ranOptionId = Utils.getRandomInt(0,(testData.options[i].length-1))

                        cy.request({
                            method: "POST",
                            url: "https://staging.quillplus.in/api/v1/practice/check-for-correct-answer",
                            headers: {
                                Authorization: token
                            },
                            body: {
                                "practice_test_id": testData.id,
                                "practice_question_id": testData.questions[i],
                                "practice_option_id": testData.options[i][ranOptionId],
                                "time_taken": 3,
                                "time_spent": 3
                            }
                        }).then((checkCorrectIncorrectResponse) => {
                            expect(checkCorrectIncorrectResponse.status).to.be.equal(200)
                            expect(checkCorrectIncorrectResponse.body.data.solution).not.null
                            expect(checkCorrectIncorrectResponse.body.data.was_correct).to.be.a('boolean')
                            expect(checkCorrectIncorrectResponse.body.data.correct_option_id).to.be.a('number').and.not.null
                        })
                    }
                })
            })
        })
    })
})