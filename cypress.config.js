const { defineConfig } = require("cypress");

module.exports = defineConfig({
  e2e: {
    setupNodeEvents(on, config) {
      // implement node event listeners here
    },
    projectId: "cd5ehn",
    watchForFileChanges: false,
    defaultCommandTimeout: 20000
  },
  env:{
    
  },
});
